#! /usr/bin/python3
# t4exp - Tools For Experiments.
# Copyright (C) 2015 - 9999 Mateus Krepsky Ludwich.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Contact: mkludwich@gmail.com
#
# A copy of GNU General Public License can be found in copying.txt file.

import sys

sys.path.append('../t4exp')

import t4exp
from t4exp import File_Observer
from t4exp import Power_Switch

def execute_experiment(image_signature):
    fake = True

    power_switch = Power_Switch()
    power_switch.switch(fake)
    output = t4exp.execute_pyserial(fake = fake, timeout = 5 * 60)
    power_switch.switch(fake)
    file_name = t4exp.store_in_file(output, '/export/outputs/epos-' + image_signature, 'out')
    print('Stored: ' + file_name)


def main():
    File_Observer('/var/ftpd/epos.img', execute_experiment)

if __name__ == '__main__':
    main()
