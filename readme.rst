
----

=============================
t4exp - Tools For Experiments
=============================

Copyright (C) 2015 - 9999 Mateus Krepsky Ludwich.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Library General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

Contact: mkludwich@gmail.com

A copy of GNU General Public License can be found in copying.txt file.

----

============
t4exp_deamon
============

Usage
-----

1. Place the OS image file (must be named epos.img) at /var/ftpd/ of the SERVER.
2. Wait for the experiment to run and check out the results at /export/outputs/

The output file name has the following format:
epos-SHA1SUM_OF_EPOS_IMAGE-TIMESTAMP.out

Suggested Architecture
----------------------

Image loading:
Internet <--S:NIC1--> SERVER <--S:NIC2--> Hub <--C:NIC--> CLIENT

Output logging:
CLIENT <--Serial--> Serial_to_USB <--USB--> SERVER

Automatic switch ON/OFF of client machine.
SERVER --Serial (only DTR and RTS pins)--> optocoupler circuit -->
CLIENT Mother_Board

Behavior
--------

Each 10 seconds, t4exp on SERVER inspects /var/ftpd/epos.img to check
whether the image has changed.
If so, it turns on the CLIENT machine that loads the OS image
through TFTP+PXE.
The CLIENT executes the experiment and its serial output is logged by
the SERVER.

An experiment execution can finish in two ways, by end-of-input or by timeout.
If the OS program ends its execution by printing the string "Bye!" on
the serial output, the SERVER understands that the experiment
execution has finished and closes the output file.
Otherwise if the experiment execution reaches a timeout (5 minutes),
the SERVER closes the output file anyway, appending the string: "TIME
OUT!" at the end of the output file.

----

