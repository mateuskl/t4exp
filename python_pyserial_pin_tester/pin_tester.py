#! /usr/bin/python3

#
# DB-9 Male
# 1 2 3 4 5
#  6 7 8 9
#
# DE-9 Female
# 5 4 3 2 1
#  9 8 7 6

# 1     CDC
# 2     Rx
# 3     Tx
# 4     DTR
# 5     GND
# 6     DSR
# 7     RTS
# 8     CTS
# 9     RI

import serial

def main():
    device = '/dev/ttyS0'
    baud_rate = 115200
    data_bits = 8
    stop_bits = 1
    ser = serial.Serial(port = device, baudrate = baud_rate, bytesize = data_bits, stopbits = stop_bits, timeout = None)
    assert(ser.isOpen())

    dtr = False
    ser.setDTR(dtr)
    rts = False
    ser.setRTS(rts)
    tx = False

    print('getDsrDtr:', ser.getDsrDtr())

    command = input('> ')
    while command != 'q':
        if command == '1':
            print('CD (RO)')
            print(ser.getCD())
        elif command == '2':
            print('Rx (?)')
            # print(ser.read())
        elif command == '3':
            print('Tx (WO)')
            if tx:
                print('1')
                ser.write(b'1')
            else:
                print('0')
                ser.write(b'0')

            tx = not tx

        elif command == '4':
            print('DTR (RW)')
            dtr = not dtr
            print(dtr)
            ser.setDTR(dtr)
        elif command == '5':
            print('GND (.)')
        elif command == '6':
            print('DSR (RO)')
            print(ser.getDSR())
        elif command == '7':
            print('RTS (RW)')
            rts = not rts
            print(rts)
            ser.setRTS(rts)
        elif command == '8':
            print('CTS (RO)')
            print(ser.getCTS())
        elif command == '9':
            print('RI (RO)')
            print(ser.getRI())
        command = input('> ')

    ser.close()

    print('Bye!')

if __name__ == '__main__':
    main()
