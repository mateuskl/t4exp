# t4exp - Tools For Experiments.
# Copyright (C) 2015 - 9999 Mateus Krepsky Ludwich.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Contact: mkludwich@gmail.com
#
# A copy of GNU General Public License can be found in copying.txt file.

import os
import sys

from t4exp import Treatment

# Specific configuration
class Configuration:
    fake_run = False
    treatments_directory = '/local/home/mateus/hyper/images'
    treatment_timeout = 300
    deployment_directory = '/var/ftpd'
    deployment_image_name = 'epos.img'
    outputs_directory = '/local/home/mateus/hyper/outputs'
    reports_directory = '/local/home/mateus/hyper/reports'
    replications = 3
    aux_exp_runner_script = '/local/home/mateus/hyper/tools/aux_exp_runner/aux_exp_runner.py'


    @staticmethod
    def treatments():
        the_treatments = []
        the_treatments.append(Treatment('tret_maxinterf', '100.img', '-tbs = 0.5 -iterations = 200'))
        the_treatments.append(Treatment('tret_medinterf', '200.img', '-tbs = 1.0 -iterations = 100'))
        # the_treatments.append(Treatment('tret_mininterf', '300.img', '-tbs = 0.1'))
        the_treatments.append(Treatment('tret_zerointerf', '401.img', '-DONTRUN'))
        return the_treatments

    @staticmethod
    def _treatments():
        the_treatments = []

        images = os.listdir(Configuration.treatments_directory)
        i = 0
        for image in images:
            if '.img' in image:
                name = 'tret' + str(i)
                treatment = Treatment(name, image, 'Aux Config!')
                the_treatments.append(treatment)
                i += 1

        return the_treatments
