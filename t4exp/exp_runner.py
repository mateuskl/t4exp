#! /usr/bin/python3
# t4exp - Tools For Experiments.
# Copyright (C) 2015 - 9999 Mateus Krepsky Ludwich.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Contact: mkludwich@gmail.com
#
# A copy of GNU General Public License can be found in copying.txt file.
from t4exp import Experiment, Experiment_Runner, Auxiliar_Experiment_Runner

from configuration import Configuration

def main():
    fake = Configuration.fake_run
    exp = Experiment(Configuration)
    treatments = exp.treatments()
    runner = Experiment_Runner(Configuration, treatments)

    for replication in range(exp.replications()):
        for treatment in treatments:
            print ('Starting one execution..........')
            auxiliar_runner = Auxiliar_Experiment_Runner(Configuration.aux_exp_runner_script, treatment.aux_exp_runner_conf())
            auxiliar_runner.start()
            runner.deploy_treatment(treatment.exp_runner_image_file())
            runner.run_experiment(fake, treatment.exp_runner_image_file(), replication, treatment, Configuration.treatment_timeout, auxiliar_runner)
            if not auxiliar_runner.has_started():
                print ('WARNING: Auxiliar runner has not started (not @RTS received) and runner has already finished. Invalidating output for this run.')
                auxiliar_runner.stop()
                runner.invalidate_run('Aux Runner Not Started')
            elif not auxiliar_runner.is_finished() or not auxiliar_runner.finished_well():
                if not auxiliar_runner.is_finished():
                    print ('WARNING: Auxiliar runner not finished. Terminating it... and Invalidating output for this run.')
                    auxiliar_runner.stop()
                    runner.invalidate_run('Aux Runner Not Finished')
                else:
                    print ('WARNING: Auxiliar runner finished with error. Invalidating output for this run.')
                    auxiliar_runner.stop()
                    runner.invalidate_run('Aux Runner Finished with error')


            print ('..........One execution finished')

    runner.process_results()

if __name__ == '__main__':
    main()
