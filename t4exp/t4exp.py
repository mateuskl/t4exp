# t4exp - Tools For Experiments.
# Copyright (C) 2015 - 9999 Mateus Krepsky Ludwich.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Contact: mkludwich@gmail.com
#
# A copy of GNU General Public License can be found in copying.txt file.

import time
import serial
import os
import subprocess
import shutil
import hashlib
from threading import Thread, Semaphore, Timer

# Experiment related
class Output_Variable:
    def __init__(self):
        self.name = ''
        self.value = None


class Treatment:
    def __init__(self, name, exp_runner_image_file, aux_exp_runner_conf):
        self.__name = name
        self.__exp_runner_image_file = exp_runner_image_file
        self.__aux_exp_runner_conf = aux_exp_runner_conf

    def name(self):
        return self.__name

    def exp_runner_image_file(self):
        return self.__exp_runner_image_file

    def aux_exp_runner_conf(self):
        return self.__aux_exp_runner_conf


class Experiment:

    def __init__(self, configuration):
        self.__configuration = configuration

    def treatments(self):
        """Return the experiment treatments (specific instances of the levels
        for the experiment factors)."""
        return self.__configuration.treatments()


    def replications(self):
        return self.__configuration.replications


class Auxiliar_Experiment_Runner(Thread):

    def __init__(self, script, configuration):
        Thread.__init__(self)
        self.__read_to_start = Semaphore(value = 0)
        self.__script = script
        self.__configuration = configuration
        self.__is_finished = False
        self.__process = None
        self.__return_code = -1
        self.__has_started = False

    def __wait_read_to_start(self):
        self.__read_to_start.acquire()

    def signal_read_to_start(self):
        self.__read_to_start.release()


    def run(self):
        self.__wait_read_to_start()

        if not self.__is_finished:
            self.__has_started = True
            print('Auxiliar_Experiment_Runner Starting')
            cmd = ['python', self.__script, self.__configuration]
            # cmd = 'ls'
            print(cmd)
            self.__process = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.STDOUT, universal_newlines = True)
            self.__process.wait()
            # print('> ', self.__process.stdout.read())

            self.__is_finished = True
            self.__return_code = self.__process.returncode
            print('Auxiliar_Experiment_Runner Finished. Return code: ', self.__return_code)


    def has_started(self):
        return self.__has_started

    def is_finished(self):
        return self.__is_finished

    def finished_well(self):
        return self.__return_code == 0

    def stop(self):
        if not self.__is_finished and self.has_started():
            assert self.__process
            self.__process.terminate()
            self.__is_finished = True

        if not self.has_started():
            self.__is_finished = True
            self.signal_read_to_start()


class Experiment_Runner:

    def __init__(self, configuration, treatments):
        self.__configuration = configuration
        self.__outputs = []
        num_treatments = len(treatments)
        num_replications = self.__configuration.replications
        self.__num_executions = num_treatments * num_replications
        self.__completed_executions = 0
        self.__treatments_map = {} # Treatment -> List of Replications Outputs
            # Each output contains the output result of all measured variables

    def deploy_treatment(self, treatment):
        """Copies the image file that represents the treatment to its deployment
        directory (e.g.a  base directory for a TFTP server), renaming the
        image file if needed."""
        shutil.copyfile(os.path.join(self.__configuration.treatments_directory, treatment), os.path.join(self.__configuration.deployment_directory, self.__configuration.deployment_image_name))

    def run_experiment(self, fake, image_file, replication, treatment, treatment_timeout, auxiliar_runner):
        """Runs the experiment for the current deployed treatment, collect and
        store the results.
        **treatment_timeout** is specified in seconds.
        """
        # power_machine_on
        power_switch = Power_Switch()
        power_switch.switch(fake)

        # setup_output_logger
        print('Logging output')
        output = execute_pyserial(fake, treatment_timeout, auxiliar_runner)
        # print('self.__configuration.outputs_directory = ', self.__configuration.outputs_directory)
        # print('image_file = ', image_file)
        assert('/' not in image_file)
        file_name = store_in_file(output, os.path.join(self.__configuration.outputs_directory, image_file + '-' + treatment.name() + '-rep_' + str(replication)), 'out')
        self.__outputs.append(file_name)
        print('Output stored in: ' + file_name)

        # power_machine_off
        power_switch.switch(fake)

        self.__completed_executions += 1
        print('Progress. Runned ' + str(self.__completed_executions) + ' of ' + str(self.__num_executions) + ' executions (treatments * replications).')

        self.__write_signature(image_file, file_name)


    def __write_signature(self, image_file_name, output_file_name):
        a_file = open(output_file_name, mode = 'a', encoding = 'utf-8')
        a_file.write('#Image file: ' + image_file_name + '\n')
        a_file.write('#sha1sum: ' + sha1sum(os.path.join(self.__configuration.treatments_directory, image_file_name)) + '\n')
        a_file.write('#This file: ' + output_file_name.split('/')[-1] + '\n')

        a_file.close()

    def invalidate_run(self, reason):
        current_output = self.__outputs[-1]
        a_file = open(current_output, mode = 'a', encoding = 'utf-8')
        a_file.write('\n#INVALID OUTPUT!\n')
        a_file.write('#' + reason)
        a_file.close()

    def process_results(self):
        """Spec: Go over all treatment outputs and generates a table from them.
        Impl: Reads all output files and generates a CVS file from them.
        """
        # TODO
        return
        # output file: IMAGE_NAME-TREATMENT_NAME-REPLICATION-YEAR-MONTH-DAY-HOURMINUTESECOND.out
        print('TODO')
        for output in self.__outputs:
            print(output.split('-'))



# Auto-test related
class Ignore_white_space_tabs_and_new_lines:

    def apply_filter(self, s):
        s = s.replace(' ', '')
        s = s.replace('\n', '')
        s = s.replace('\t', '')

        return s


class Ignore_tcpdump_timestamp:

    def apply_filter(self, s):
        # Very dummy implementation
        lines = s.split('\n')
        result = ''
        for line in lines:
            if not 'length' in line: # it is a line with timestamp
                result += line

        return result


def check_output(expected: str, obtained: str, filters: list):

    clean_expected = expected
    clean_obtained = obtained

    for filt in filters:
        clean_expected = filt.apply_filter(clean_expected)
        clean_obtained = filt.apply_filter(clean_obtained)

    if (clean_expected != clean_obtained):
        print('Failure: outputs differ.')
        print('Expected:')
        print (expected)
        print('Obtained:')
        print (obtained)

    assert(clean_expected == clean_obtained)


# ----

# File related
def sha1sum(file_name, encoding = 'utf-8'):
    if encoding:
        a_file = open(file_name, mode = 'r', encoding = encoding)
    else:
        a_file = open(file_name, mode = 'rb')

    content = a_file.read()

    if encoding:
        content_bytes = content.encode(encoding)
    else:
        content_bytes = content

    return hashlib.sha1(content_bytes).hexdigest()

def store_in_file(content: str, file_name_prefix: str, file_name_suffix: str, date = True):

    if date:
        file_name = file_name_prefix + '-' + time.strftime('%Y-%m-%d-%H%M%S') + '.' + file_name_suffix
    else:
        file_name = file_name_prefix + '.' + file_name_suffix

    a_file = open(file_name, mode = 'w', encoding = 'utf-8')
    a_file.write(content)
    a_file.close()
    return file_name


class File_Observer:

    def __init__(self, file_name, handler, ignore_if_absent = True):
        assert(file_name.__class__ == str)
        self.__file_name = file_name
        self.__file_signature = sha1sum(self.__file_name, None)
        self.__handler = handler
        self.__observe(ignore_if_absent)

    def __observe(self, ignore_if_absent):
        while True:
            time.sleep(10)
            if ignore_if_absent:
                if not os.path.isfile(self.__file_name):
                    continue

            file_signature = sha1sum(self.__file_name, None)
            if self.__file_signature != file_signature:
                self.__file_signature = file_signature
                self.__handler(self.__file_signature)

# ----

# Time-related
class Timeout:

    def __init__(self, time_to_wait):
        self.__timed_out = False
        self.__time_to_wait = time_to_wait
        self.__timer = Timer(self.__time_to_wait, self.__do_timeout)

    def __do_timeout(self):
        self.__timed_out = True

    def has_happened(self):
        return self.__timed_out

    def time_to_wait(self):
        return self.__time_to_wait

    def start(self):
        self.__timer.start()

    def cancel(self):
        self.__timer.cancel()


class Alarm:

    def __init__(self, interval, handler):
        self.__interval = interval
        self.__timer = Timer(self.__interval, handler)
        self.__start()

    def __start(self):
        self.__timer.start()


# ----

# Pyserial related
class Logger:
    def __init__(self, time_to_wait = None):
        self.__timeout = Timeout(time_to_wait)


    def __set_timeout(self):
        if self.__timeout.time_to_wait():
            print('Setting time out for Logger of ', self.__timeout.time_to_wait(), ' seconds.')
            self.__timeout.start()
        else:
            print('No timeout set.')


    def __unset_timeout(self):
        if self.__timeout.time_to_wait():
            self.__timeout.cancel()


    def execute_pyserial(self, fake = False, auxiliar_runner = None):
        self.__set_timeout()

        output = ''

        if not fake:
            device = '/dev/ttyUSB0'
            baud_rate = 115200
            data_bits = 8
            stop_bits = 1
            ser = serial.Serial(port = device, baudrate = baud_rate, bytesize = data_bits, stopbits = stop_bits, timeout = None)
            print(ser)
            assert(ser.isOpen())

            data = None
            str_data = ''
            while ('Bye!' not in str_data) and (not self.__timeout.has_happened()):
                try:
                    in_waiting = ser.inWaiting()
                    if in_waiting:
                        data = ser.read(in_waiting)
                        str_data = data.decode('utf-8')
                        output += str_data
                        if '@RTS' in str_data: # Not to be confused with read-to-send of Serial protocol
                            if auxiliar_runner:
                                # print('Signal read-to-start to aux runner')
                                auxiliar_runner.signal_read_to_start()

                except serial.serialutil.SerialException:
                    pass
                except UnicodeDecodeError:
                    pass
                except KeyboardInterrupt:
                    break

            ser.close()

        else:
            print('Reading serial...')
            time.sleep(1)
            if auxiliar_runner:
                auxiliar_runner.signal_read_to_start()
            time.sleep(1)
            print('done reading serial')
            output =  'Fake Output ;)\n'

        if not self.__timeout.has_happened():
            self.__unset_timeout()
            print("Logger exited by end-of-input")
        else:
            output = output + '\n#TIME OUT!\n'
            print("Logger exited by timeout")

        return output


def execute_pyserial(fake = False, timeout = None, auxiliar_runner = None):
    logger = Logger(timeout)
    return logger.execute_pyserial(fake, auxiliar_runner)

# ----

# Serial Power Switch
class Power_Switch:

    def __open(self):
        device = '/dev/ttyS0'
        baud_rate = 115200
        data_bits = 8
        stop_bits = 1
        self.__ser = serial.Serial(port = device, baudrate = baud_rate, bytesize = data_bits, stopbits = stop_bits, timeout = None)
        assert(self.__ser.isOpen())
        self.__ser.setDTR(False)
        self.__ser.setRTS(False)

    def switch(self, fake = False):
        if fake:
            print('switch ON/OFF')
            return

        self.__open()
        self.__ser.setRTS(True)
        time.sleep(1) # Second
        self.__ser.setRTS(False)
        time.sleep(1) # Maybe not nedded
        self.__ser.close()


# ----
