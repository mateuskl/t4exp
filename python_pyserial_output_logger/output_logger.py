#! /usr/bin/python3

# Author: Mateus Krepsky Ludwich.
# Contact: mateus@lisha.ufsc.br
# Year: 2015

import sys
sys.path.append('../t4exp')

import t4exp


# Just for debugging.
def execute_pyserial():
    return 'Hello World.'


def main():
    print('Output Logger')

    output = t4exp.execute_pyserial()
    file_name = ''

    if len(sys.argv) == 1:
        file_name = t4exp.store_in_file(output, 'epos', 'out')
    elif len(sys.argv) == 2 and '.img' in sys.argv[1]:
        name = sys.argv[1].replace('.img', '')
        file_name = t4exp.store_in_file(output, name, 'out', date = False)

    if file_name:
        print('Stored: ' + file_name)

if __name__ == '__main__':
    main()
